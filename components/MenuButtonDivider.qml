import QtQuick 2.9

import "." as MoneroDollarComponents
import "effects/" as MoneroDollarEffects

Rectangle {
    color: MoneroDollarComponents.Style.appWindowBorderColor
    height: 1

    MoneroDollarEffects.ColorTransition {
        targetObj: parent
        blackColor: MoneroDollarComponents.Style._b_appWindowBorderColor
        whiteColor: MoneroDollarComponents.Style._w_appWindowBorderColor
    }
}
