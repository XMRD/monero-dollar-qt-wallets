import QtQuick 2.9
import QtQuick.Layouts 1.1

import "../components" as MoneroDollarComponents

ColumnLayout {
    property alias buttonText: button.text
    property alias description: description.text
    property alias title: title.text
    signal clicked()

    id: settingsListItem
    Layout.fillWidth: true
    spacing: 0

    Rectangle {
        // divider
        Layout.preferredHeight: 1
        Layout.fillWidth: true
        Layout.bottomMargin: 8
        color: MoneroDollarComponents.Style.dividerColor
        opacity: MoneroDollarComponents.Style.dividerOpacity
    }

    RowLayout {
        Layout.fillWidth: true
        spacing: 0

        ColumnLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignVCenter
            spacing: 0

            MoneroDollarComponents.TextPlain {
                id: title
                Layout.fillWidth: true
                Layout.preferredHeight: 20
                Layout.topMargin: 8
                color: MoneroDollarComponents.Style.defaultFontColor
                opacity: MoneroDollarComponents.Style.blackTheme ? 1.0 : 0.8
                font.bold: true
                font.family: MoneroDollarComponents.Style.fontRegular.name
                font.pixelSize: 16
            }

            MoneroDollarComponents.TextPlainArea {
                id: description
                color: MoneroDollarComponents.Style.dimmedFontColor
                colorBlackTheme: MoneroDollarComponents.Style._b_dimmedFontColor
                colorWhiteTheme: MoneroDollarComponents.Style._w_dimmedFontColor
                Layout.fillWidth: true
                horizontalAlignment: TextInput.AlignLeft
            }
        }

        MoneroDollarComponents.StandardButton {
            id: button
            small: true
            onClicked: {
                settingsListItem.clicked()
            }
            width: 135
        }
    }
}
