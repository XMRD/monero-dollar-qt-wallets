import QtQuick 2.9

import "../components" as MoneroDollarComponents

TextEdit {
    color: MoneroDollarComponents.Style.defaultFontColor
    font.family: MoneroDollarComponents.Style.fontRegular.name
    selectionColor: MoneroDollarComponents.Style.textSelectionColor
    wrapMode: Text.Wrap
    readOnly: true
    selectByMouse: true
    // Workaround for https://bugreports.qt.io/browse/QTBUG-50587
    onFocusChanged: {
        if(focus === false)
            deselect()
    }
}
